package com.yuzhua.websocket.qianlang.message;

import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;

public class SocketEvent {

    String cmd;

    WSAbstractEvent data;

    public SocketEvent(String cmd) {
        this.cmd = cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public WSAbstractEvent getData() {
        return data;
    }

    public void setData(WSAbstractEvent data) {
        this.data = data;
    }

    public String getCmd() {
        return cmd;
    }
}
