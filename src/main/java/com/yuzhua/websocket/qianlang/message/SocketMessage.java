package com.yuzhua.websocket.qianlang.message;

public class SocketMessage {

    String cmd;

    WSMessage data;

    public void setData(WSMessage data) {
        this.data = data;
    }

    public WSMessage getData() {
        return data;
    }

    public SocketMessage(String cmd) {
        this.cmd = cmd;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }
}
