package com.yuzhua.websocket.qianlang.message.event;

import java.util.List;

public class WSChatEvent extends WSAbstractEvent {

    List<String> chatIds;

    public List<String> getChatIds() {
        return chatIds;
    }

    public WSChatEvent(String messageType, String toUser, String fromUser, String timestamp, List<String> chatIds) {
        super(messageType, toUser, fromUser, timestamp);
        this.chatIds = chatIds;
    }

    public void setChatIds(List<String> chatIds) {
        this.chatIds = chatIds;
    }
}
