package com.yuzhua.websocket.qianlang.message.event;

public class WSAbstractEvent {

    String messageType;

    String toUser;

    String fromUser;

    String timestamp;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public WSAbstractEvent(String messageType, String toUser, String fromUser, String timestamp) {
        this.messageType = messageType;
        this.toUser = toUser;
        this.fromUser = fromUser;
        this.timestamp = timestamp;
    }
}
