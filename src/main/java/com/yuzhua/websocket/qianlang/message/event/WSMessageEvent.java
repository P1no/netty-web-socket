package com.yuzhua.websocket.qianlang.message.event;

import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;

import java.util.List;

public class WSMessageEvent extends WSAbstractEvent {

    List<String> messageIds;

    public List<String> getMessageIds() {
        return messageIds;
    }

    public void setMessageIds(List<String> messageIds) {
        this.messageIds = messageIds;
    }

    public WSMessageEvent(String messageType, String toUser, String fromUser, String timestamp, List<String> messageIds) {
        super(messageType, toUser, fromUser, timestamp);
        this.messageIds = messageIds;
    }
}

