package com.yuzhua.websocket.qianlang.message;

public class WSMessage {

//    /** 消息类型（事件还是消息还是通知）
//     * 0：连接
//     * 1：消息发送
//     * 2：已读事件
//     * 3：输入中事件
//     * 4：离线事件
//     * 5：强制下线事件
//     * 6：系统通知
//     * 9：心跳ping
//     * 10：心跳pong
//      */
//    Integer type;

//    // 消息ID
//    String id;

//    // 会话ID
//    String chatId;

    // 消息发送者ID
    String fromUser;

    // 消息接受者ID
    String toUser;

    // 秒级时间戳
    String timestamp;

    // 消息的发送ID
    String sendID;

    String msg_type;

    String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSendID() {
        return sendID;
    }

    public void setSendID(String sendID) {
        this.sendID = sendID;
    }

    public WSMessage(String fromUser, String toUser, String timestamp, String sendID, String msg_type, String content) {
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.timestamp = timestamp;
        this.sendID = sendID;
        this.msg_type = msg_type;
        this.content = content;
    }
}
