package com.yuzhua.websocket.qianlang.channel;

import io.netty.channel.Channel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserChannelMap {

    private static Map<String, NioSocketChannel> userChannelMap = new ConcurrentHashMap<String, NioSocketChannel>(1024);;

    /**
     * 添加用户到channel中
     * @param userId
     * @param channel
     */
    public static void put(String userId, NioSocketChannel channel) { userChannelMap.put(userId, channel); }

    /**
     * 在channels中删除用户
     * @param userId
     */
    public static void remove(String userId) {
        userChannelMap.remove(userId);
    }

    /**
     * 通过用户id取得用户的通讯channel
     * @param userId
     * @return
     */
    public static NioSocketChannel getChannelById(String userId) {

        return userChannelMap.get(userId);
    }

    /**
     * 移除通信管道
     * @param channel
     */
    public static void removeChannelByChannel(NioSocketChannel channel) {

        userChannelMap.entrySet().stream().filter( entry -> entry.getValue() == channel).forEach( entry -> userChannelMap.remove(entry.getKey()));
    }

    /**
     * 获取用户ID
     * @param channel
     * @return
     */
    public static String getUserId(NioSocketChannel channel) {

        return userChannelMap.entrySet().stream().filter( entry -> entry.getValue() == channel).findFirst().get().getKey();
    }
}
