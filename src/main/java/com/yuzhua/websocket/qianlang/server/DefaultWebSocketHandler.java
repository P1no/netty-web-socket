package com.yuzhua.websocket.qianlang.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yuzhua.websocket.qianlang.channel.UserChannelMap;
import com.yuzhua.websocket.qianlang.message.SocketEvent;
import com.yuzhua.websocket.qianlang.message.SocketMessage;
import com.yuzhua.websocket.qianlang.message.WSMessage;
import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;
import com.yuzhua.websocket.qianlang.message.event.WSChatEvent;
import com.yuzhua.websocket.qianlang.message.event.WSMessageEvent;
import com.yuzhua.websocket.qianlang.service.FromUserService;
import com.yuzhua.websocket.qianlang.service.FromUserServiceImpl;
import com.yuzhua.websocket.qianlang.service.ToUserService;
import com.yuzhua.websocket.qianlang.service.ToUserServiceImpl;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultWebSocketHandler extends SimpleChannelInboundHandler<WebSocketFrame> {

    private WebSocketServerHandshaker handshaker;

    @Autowired
    private FromUserService fromUserService = new FromUserServiceImpl();

    @Autowired
    private ToUserService toUserService = new ToUserServiceImpl();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (msg instanceof FullHttpRequest) {

            String uri = ((FullHttpRequest) msg).uri().toString();

            uri = uri.replace("/im?", "");

            String[] params = uri.split("&");

            for (String param: params) {

                String[] KeyValue = param.split("=");
                if (KeyValue[0].equals("userId")) {
                    UserChannelMap.put(KeyValue[1], (NioSocketChannel) ctx.channel());
                    System.out.println(UserChannelMap.getUserId((NioSocketChannel) ctx.channel()));
                }
            }

            handleHTTPRequest(ctx, (FullHttpRequest) msg);
        } else if (msg instanceof WebSocketFrame) {

            handleWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
    }

    private void handleWebSocketFrame(ChannelHandlerContext ctx,
                                      WebSocketFrame frame) throws UnsupportedEncodingException {

        if (frame instanceof TextWebSocketFrame) {


        } else if (frame instanceof CloseWebSocketFrame) {

        } else if (frame instanceof PingWebSocketFrame) {

            ctx.writeAndFlush(new PongWebSocketFrame(Unpooled.copiedBuffer(new byte[0x1])));

        } else if (frame instanceof BinaryWebSocketFrame) {

            ByteBuf buf = ((BinaryWebSocketFrame) frame).content();
            byte[] readBytes = new byte[buf.readableBytes()];
            buf.readBytes(readBytes);

            String content = ((BinaryWebSocketFrame) frame).toString();

            String body = new String(readBytes,"UTF-8");

            JSONObject jsonObject = JSONObject.parseObject(body);
            JSONObject data = (JSONObject) jsonObject.get("data");

            String toUser;
            String fromUser;

            switch ((String) jsonObject.get("cmd")) {
                case "message.send":
                    WSMessage dataMessage = data.toJavaObject(WSMessage.class);

                    // 获取目标用户ID
                    toUser = dataMessage.getToUser();
                    // 获取发送者用户ID
                    fromUser = dataMessage.getFromUser();

                    toUserService.sendMessage(toUser, dataMessage);
                    fromUserService.sendEcho(fromUser, dataMessage);
                    break;
                case "event.trigger.read.message":
                    WSMessageEvent eventMessage = data.toJavaObject(WSMessageEvent.class);
                    JSONArray messageIds = data.getJSONArray("msg_id");
                    eventMessage.setMessageIds(messageIds.toJavaList(String.class));

                    toUser = eventMessage.getToUser();

                    toUserService.sendReadedEvent(toUser, eventMessage);
                    break;
                case "event.trigger.read.chat":
                    WSChatEvent chatEvent = data.toJavaObject(WSChatEvent.class);
                    JSONArray chatIds = data.getJSONArray("chat_id");
                    chatEvent.setChatIds(chatIds.toJavaList(String.class));

                    toUser = chatEvent.getToUser();

                    toUserService.sendReadedEvent(toUser, chatEvent);
                    break;
                case "event.receive":
                    WSAbstractEvent receiveEvent = data.toJavaObject(WSAbstractEvent.class);

                    fromUser = receiveEvent.getFromUser();
                    toUser = receiveEvent.getToUser();

                    break;

            }

        } else if (frame instanceof ContinuationWebSocketFrame) {

        }
    }

    private void handleHTTPRequest(ChannelHandlerContext ctx,
                                   FullHttpRequest req) throws Exception {

        if (!req.decoderResult().isSuccess() || (!"websocket".equals(req.headers().get("Upgrade")))) {

            sendHTTPResponse(ctx, req, new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));

            return;
        }

        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory("ws://localhost:8088/im", null, false);
        handshaker = wsFactory.newHandshaker(req);

        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }
    }

    private void sendHTTPResponse(ChannelHandlerContext ctx,
                                  FullHttpRequest req, FullHttpResponse res) {
        if (res.status().code() != 200) {
            ByteBuf buf = Unpooled.copiedBuffer(res.status().toString(), CharsetUtil.UTF_8);
            res.content().writeBytes(buf);
            buf.release();
        }

        ChannelFuture f = ctx.channel().writeAndFlush(res);

        if (!isKeepAlive(req) || res.status().code() != 200) {
            f.addListener(ChannelFutureListener.CLOSE);
        }
    }

    private boolean isKeepAlive(FullHttpRequest req) {

        if (req.headers().get("Connection").equals("keep-alive")) {
            return true;
        }

        return false;
    }

    private void setContentLength(FullHttpResponse res, int length) {

        res.headers().add("Content-Length", length);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, WebSocketFrame webSocketFrame) throws Exception {

        if (webSocketFrame instanceof TextWebSocketFrame) {

            System.out.println(webSocketFrame);
        } else if (webSocketFrame instanceof FullHttpRequest) {

            System.out.println(webSocketFrame);
        }
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);

        System.out.println(ctx.channel());

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        System.out.println(ctx.channel());
        ctx.writeAndFlush(new TextWebSocketFrame("123123"));
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {

        super.channelUnregistered(ctx);

        UserChannelMap.removeChannelByChannel((NioSocketChannel) ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);

        UserChannelMap.removeChannelByChannel((NioSocketChannel) ctx.channel());
    }
}
