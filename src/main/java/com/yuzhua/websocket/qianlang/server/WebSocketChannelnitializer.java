package com.yuzhua.websocket.qianlang.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocket08FrameDecoder;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import jdk.nashorn.internal.objects.annotations.Constructor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: 通道初始化器器
 * 用来加载通道处理器(channelhandler)
 * @Author: lyl
 * @Date: 2020/12/18 12:18
 */

class WebSocketChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final String KEY_STORE_PATH = System.getProperty("user.home") + File.separator + "leiyongl_server_ks";
    private static final String STORE_TYPE = "PKCS12";
    private static final String PROTOCOL = "TLS";
    private static final String KEY_STORE_PASSWORD = "123456";
    private static final String KEY_PASSWORD = KEY_STORE_PASSWORD;

    private static boolean OPEN_SSL = false;

    public WebSocketChannelInitializer(boolean openSsl) {

        OPEN_SSL = openSsl;
    }

    /**
    * 初始化通道
    * 在这个方法中加载对应的ChannelHandler
    * */
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {

        System.out.println(socketChannel.remoteAddress());
        /*
        * 获取管道,将一个个ChannelHandler添加到管道中
        * 可以将channelpipeline理解为拦截器
        * 当我们的socketChannel数据进来时候会依次调用我们的ChannelHandler
        * */

        // 添加一个http解码器
        socketChannel.pipeline().addLast("http-codec", new HttpServerCodec());

        // 添加大数据流的支持
        socketChannel.pipeline().addLast("aggregator", new HttpObjectAggregator(65536));

        // 添加聚合器 ,可以将我们的httpmaessage聚合成Fullhttprequest/respond ---想拿到请求和响应就要添加聚合器
        socketChannel.pipeline().addLast("http-chunked", new ChunkedWriteHandler());

        socketChannel.pipeline().addLast(new WebSocketServerCompressionHandler());

//        socketChannel.pipeline().addLast(new WebSocketServerProtocolHandler("/im", null, true));

        // 添加解码器
        socketChannel.pipeline().addLast(new WebSocket08FrameDecoder(false, false, 65536));

        socketChannel.pipeline().addLast(new IdleStateHandler(3, 0, 0, TimeUnit.SECONDS));

        //添加自定义的handler进行业务处理
        socketChannel.pipeline().addLast("websocket-handler", new DefaultWebSocketHandler());

        if (OPEN_SSL) {

            socketChannel.pipeline().addLast(createSslHandlerUsingRawApi());
        }

    }

    private static ChannelHandler createSslHandlerUsingRawApi() throws Exception {

        KeyStore keyStore = KeyStore.getInstance(STORE_TYPE);

        keyStore.load(new FileInputStream(KEY_STORE_PATH), KEY_STORE_PASSWORD.toCharArray());

        // keyManagerFactory
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, KEY_PASSWORD.toCharArray());

        // trustManagerFactory
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        // sslContext
        SSLContext sslContext = SSLContext.getInstance(PROTOCOL);
        sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);

        SSLEngine sslEngine = sslContext.createSSLEngine();
        sslEngine.setUseClientMode(false);
        return new SslHandler(sslEngine);
    }
}
