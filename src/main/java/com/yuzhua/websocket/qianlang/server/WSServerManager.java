package com.yuzhua.websocket.qianlang.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ssl.SslHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

/*
 * 应用主从线程池的理念
 * */

public class WSServerManager extends WSServer implements Runnable {

    private static final String HOST = "localhost";
    private static final int PORT = 8088;
    private static final boolean OPEN_SSL = true;

    /*
     * 主线程池
     * */
    private final EventLoopGroup bossGroup = new NioEventLoopGroup();

    /*
     * 工作线程池
     * */
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();

    /*
     * 服务器
     * */
    private final ServerBootstrap server = new ServerBootstrap();

    /*
     * 回调
     * */
    private ChannelFuture future;

    @Override
    public void run() {
        start();
    }

    @Override
    public void start() {

        server.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 1024)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childHandler(new WebSocketChannelInitializer(false));

        try {

            future = server.bind(PORT).sync();

            future.channel().closeFuture().sync();

        } catch (Exception e) {

            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
            e.printStackTrace();
        }
    }

}
