package com.yuzhua.websocket.qianlang.coder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrameDecoder;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class NettyMessageDecoder extends ByteToMessageDecoder implements WebSocketFrameDecoder {

    private static int FIN_MASK = 0x80;
    private static int OPCODE_MASK = 0x0F;
    private static int RSV_MASK = 0x70;
    private static int RSV1_MASK = 0x40;
    private static int MASK_MASK = 0x80;
    private static int PAYLOAD_LEN_MASK = 0x7F;

    private static ByteBuf buf = Unpooled.buffer();

    public NettyMessageDecoder() {
    }

    enum State {
        READING_FIN,
        READING_OPCODE,
        READING_MASK_AND_LEN,
        READING_PAYLOAD,
        CORRUPT
    }

    private static final byte OPCODE_CONT = 0x0;
    private static final byte OPCODE_TEXT = 0x1;
    private static final byte OPCODE_BINARY = 0x2;
    private static final byte OPCODE_CLOSE = 0x8;
    private static final byte OPCODE_PING = 0x9;
    private static final byte OPCODE_PONG = 0xA;

    private final long maxFramePayloadLength = 65536;
    private final boolean allowExtensions = false;
    private final boolean expectMaskedFrames = false;
    private final boolean allowMaskMismatch = false;
    private int fragmentedFramesCount;
    private boolean frameFinalFlag;
    private boolean frameMasked;
    private int frameRsv;
    private int frameOpcode;
    private long framePayloadLen1;
    private boolean receivedClosingHandshake;

    private static State state = State.READING_FIN;

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {

        if (receivedClosingHandshake) {
            byteBuf.skipBytes(actualReadableBytes());
            return;
        } else {

            Object obj = decode(channelHandlerContext, byteBuf);
        }
    }

    /** |  0    1    2    3   4 ~ 7  |  0      1 ~ 7     |    0 ~ 7     0 ~ 7      |     （一行 `4` 个字节，一个字节 `8` bit）
     *  | FIN RSV1 RSV2 RSV3  Opcode | MASK  payload_len |  if payload_len == 126  |
     *
     *  |      if payload_len == 127   (则多出8个字节，加上126的那两个字节)              |
     *
     *  |payload_len == 127(则多出8个字节，加上126的那两个字节)|                         |
     *
     *  | 这 4 个字节用于 MASK KEY 将会用它来解析数据          |   Payload Data          |
     *
     *  | Payload Data                                                              |
     *
     *  | Payload Data                                                              |
     */

    private Object decode(ChannelHandlerContext ctx,
                          ByteBuf in) {

        switch (state) {
            case READING_FIN:
                /**
                 * 读取消息帧的第一个字节
                 * 是否是最后一帧 和 OPCODE
                 */
                if (!in.isReadable()) {
                    return null;
                }
                framePayloadLen1 = 0;
                // FIN, RSV, OPCODE
                //读取ws帧的第一个字节，解析出FIN  RSV OPCODE
                byte b = in.readByte();
                frameFinalFlag = (b & FIN_MASK) != 0;
                frameRsv = (b & RSV_MASK) >> 4;
                frameOpcode = b & OPCODE_MASK;

                state = State.READING_MASK_AND_LEN;

            case READING_MASK_AND_LEN:
                /**
                 * 读取消息帧的第二个字节
                 * isMASK 和 消息内容长度
                 */
                if (!in.isReadable()) {
                    return null;
                }

                b = in.readByte();

                frameMasked = (b & MASK_MASK) != 0;
                framePayloadLen1 = b & PAYLOAD_LEN_MASK;

                if (frameRsv != 0 && !allowExtensions) {
                    protocolViolation(ctx, "this protocol not allowed extension param");
                    return null;
                }

                if (!allowMaskMismatch && expectMaskedFrames != frameMasked) {
                    protocolViolation(ctx, "received a frame that is not masked as expected");
                    return null;
                }



        }

        return null;
    }

    /**
     * 截取字节
     * @param bytes
     * @param begin
     * @param count
     * @return
     */
    public static byte[] subBytes(byte[] bytes, int begin, int count) {

        byte[] bs = new byte[count];

        for ( int i = begin; i < begin + count; i ++) {
            bs[i - begin] = bytes[i];
        }

        return bs;
    }

    /**
     * 字节转整型
     * @param bytes
     * @return
     */
    public int ByteToInt(byte[] bytes) {
        return 0;
    }

    /**
     * 协议不合法的处理
     * @param ctx
     * @param reason
     */
    private void protocolViolation(ChannelHandlerContext ctx,
                                   String reason) {
        protocolViolation(ctx, new CorruptedFrameException(reason));
    }

    /**
     * 抛出不合法的异常原因
     * @param ctx
     * @param ex
     */
    private void protocolViolation(ChannelHandlerContext ctx,
                                   CorruptedFrameException ex) {

        state = State.CORRUPT;

        if (ctx.channel().isActive()) {

            Object closeMessage;

            if (receivedClosingHandshake) {
                closeMessage = Unpooled.EMPTY_BUFFER;
            } else {
                closeMessage = new CloseWebSocketFrame(1002, null);
            }

            ctx.writeAndFlush(closeMessage).addListener(ChannelFutureListener.CLOSE);
        }

        throw ex;
    }

}
