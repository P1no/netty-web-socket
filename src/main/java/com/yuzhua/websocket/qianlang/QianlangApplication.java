package com.yuzhua.websocket.qianlang;

import com.yuzhua.websocket.qianlang.server.WSServerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class QianlangApplication {

    private static Thread nettyThread;

    public static void main(String[] args) {

        nettyThread = new Thread(new WSServerManager());
        nettyThread.start();

        SpringApplication.run(QianlangApplication.class, args);
    }

}
