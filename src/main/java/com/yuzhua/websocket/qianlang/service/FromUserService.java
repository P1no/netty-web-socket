package com.yuzhua.websocket.qianlang.service;

import com.yuzhua.websocket.qianlang.channel.UserChannelMap;
import com.yuzhua.websocket.qianlang.message.WSMessage;
import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;
import io.netty.buffer.Unpooled;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import org.springframework.stereotype.Service;

public interface FromUserService {

    void sendPong(String fromUser);

    void sendEcho(String fromUser, WSMessage message);
}
