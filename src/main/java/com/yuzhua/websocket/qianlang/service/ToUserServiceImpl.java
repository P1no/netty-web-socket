package com.yuzhua.websocket.qianlang.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yuzhua.websocket.qianlang.channel.UserChannelMap;
import com.yuzhua.websocket.qianlang.message.SocketEvent;
import com.yuzhua.websocket.qianlang.message.SocketMessage;
import com.yuzhua.websocket.qianlang.message.WSMessage;
import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;
import com.yuzhua.websocket.qianlang.message.event.WSMessageEvent;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;


public class ToUserServiceImpl implements ToUserService {

    @Override
    public void sendMessage(String toUser, WSMessage message) {
        NioSocketChannel channel = UserChannelMap.getChannelById(toUser);

        SocketMessage sendMessage = new SocketMessage("message.received");
        sendMessage.setData(message);

        byte[] sendBytes = JSON.toJSONBytes(sendMessage, SerializerFeature.WriteNullStringAsEmpty);
        ByteBuf sendBuf = Unpooled.copiedBuffer(sendBytes);

        if (channel != null) {

            channel.writeAndFlush(new BinaryWebSocketFrame(sendBuf));
        } else {

            //TODO: 存储消息，以供下次连接时拉取
        }
    }

    @Override
    public void sendReadedEvent(String toUser, WSAbstractEvent message) {

        NioSocketChannel channel = UserChannelMap.getChannelById(toUser);

        SocketEvent sendEvent = new SocketEvent("event.trigger.message");
        sendEvent.setData((WSMessageEvent) message);

        byte[] eventBytes = JSON.toJSONBytes(sendEvent, SerializerFeature.WriteNullStringAsEmpty);
        ByteBuf eventBuf = Unpooled.copiedBuffer(eventBytes);

        if (channel != null) {
            channel.writeAndFlush(new BinaryWebSocketFrame(eventBuf));
        } else {

            //TODO: 更改数据库中消息的状态
        }
    }

    @Override
    public void sendReceivedEvent(String toUser, WSAbstractEvent message) {

        NioSocketChannel channel = UserChannelMap.getChannelById(toUser);

        SocketEvent receivedEvent = new SocketEvent("event.receive");
        receivedEvent.setData(message);

        byte[] eventBytes = JSON.toJSONBytes(receivedEvent, SerializerFeature.WriteNullStringAsEmpty);
        ByteBuf receivedBuf = Unpooled.copiedBuffer(eventBytes);

        if (channel != null) {

            channel.writeAndFlush(new BinaryWebSocketFrame(receivedBuf));
        } else {


        }
    }
}
