package com.yuzhua.websocket.qianlang.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yuzhua.websocket.qianlang.channel.UserChannelMap;
import com.yuzhua.websocket.qianlang.message.SocketMessage;
import com.yuzhua.websocket.qianlang.message.WSMessage;
import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import org.springframework.stereotype.Service;


public class FromUserServiceImpl implements FromUserService {

    @Override
    public void sendPong(String fromUser) {
        NioSocketChannel channel = UserChannelMap.getChannelById(fromUser);

        channel.writeAndFlush(new PongWebSocketFrame(Unpooled.copiedBuffer(new byte[0xA])));
    }

    @Override
    public void sendEcho(String fromUser, WSMessage message) {

        NioSocketChannel channel = UserChannelMap.getChannelById(fromUser);

        SocketMessage echoMessage = new SocketMessage("message.echo");
        echoMessage.setData(message);

        byte[] echoBytes = JSON.toJSONBytes(echoMessage, SerializerFeature.WriteNullStringAsEmpty);
        ByteBuf echoBuf = Unpooled.copiedBuffer(echoBytes);

        if (channel != null) {

            channel.writeAndFlush(new BinaryWebSocketFrame(echoBuf));
        }
    }
}
