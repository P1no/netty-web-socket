package com.yuzhua.websocket.qianlang.service;

import com.yuzhua.websocket.qianlang.channel.UserChannelMap;
import com.yuzhua.websocket.qianlang.message.SocketMessage;
import com.yuzhua.websocket.qianlang.message.WSMessage;
import com.yuzhua.websocket.qianlang.message.event.WSAbstractEvent;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.springframework.stereotype.Service;

public interface ToUserService {

    void sendMessage(String toUser, WSMessage message);

    void sendReadedEvent(String toUser, WSAbstractEvent message);

    void sendReceivedEvent(String toUser, WSAbstractEvent message);
}
